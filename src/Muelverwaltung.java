import java.beans.Statement;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Scanner;
import java.util.logging.Level;

public class Muelverwaltung {


	private static Connection c = null;
	public static java.sql.Statement stmt=null;


	public static void main(String[] args) {

		String url = "jdbc:mysql://localhost:3306";
		String user = "root";
		Scanner sc= new Scanner(System.in);
		System.out.println("Paasswort bitte eingeben: ");
		String pw = sc.nextLine();
		String password = pw;   


		try
		{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Creating DBConnection"); 
			c = DriverManager.getConnection(url, user, password); 
			
			stmt=c.createStatement();
			String database="CREATE DATABASE IF NOT EXISTS muellunternehmen;";
			stmt.executeUpdate(database);
			stmt.close();
			
			stmt=c.createStatement();
			database="USE muellunternehmen";
			stmt.executeUpdate(database);
			stmt.close();

			createTableMueltonne();
			createTableMuellaster();
			createTableHerstellerMueltonne();
			createTableHerstellerMuellaster();
			createTableStadtbereich();
			getValues();

			c.close();

		}
		catch(Exception e)
		{
			System.err.println("Couldn't create DBConnection"); 
			System.err.println(e.getClass().getName()+": "+ e.getMessage());
			System.exit(0);
		}
	}

	public static void createTableMueltonne() throws SQLException
	{
		stmt =  c.createStatement();

		String tableMueltonne =
				"CREATE TABLE if not exists mueltonne "
						+ "(idmueltonne INT PRIMARY KEY NOT NULL, "
						+ "modell TINYTEXT, "
						+ "fassungsvermoegen SMALLINT NOT NULL, "
						+ "hersteller TINYINT NOT NULL);";

		System.out.println("created table mueltonne successfully");
		stmt.executeUpdate(tableMueltonne);
		stmt.close();
	}

	public static void createTableMuellaster() throws SQLException
	{
		stmt = c.createStatement();

		String tableMuellaster =
				"CREATE TABLE if not exists muellaster "
						+ "(lasterID TINYINT PRIMARY KEY NOT NULL, "
						+ "kennzeichen TEXT, "
						+ "nichtAusleerbareMueltonnen INT, "
						+ "hersteller TINYINT NOT NULL, "
						+ "fahrenderStadtbereich SMALLINT NOT NULL);";

		System.out.println("created table muellaster successfully");
		stmt.executeUpdate(tableMuellaster);
		stmt.close();
	}

	public static void createTableHerstellerMueltonne() throws SQLException
	{
		stmt = c.createStatement();

		String tableHerstellerMueltonne =
				"CREATE TABLE if not exists herstellerMueltonne "
						+ "(idhersteller TINYINT PRIMARY KEY NOT NULL, "
						+ "name VARCHAR(50), "
						+ "PLZ SMALLINT NOT NULL, "
						+ "adresse VARCHAR(100), "
						+ "kontakt VARCHAR(30));";

		System.out.println("created table herstellerMueltonne successfully");
		stmt.executeUpdate(tableHerstellerMueltonne);
		stmt.close();
	}

	public static void createTableHerstellerMuellaster() throws SQLException
	{
		stmt = c.createStatement();

		String tableHerstellerLaster =
				"CREATE TABLE if not exists herstellerLaster "
						+ "(idherstellerLaster TINYINT PRIMARY KEY NOT NULL, "
						+ "name VARCHAR(50), "
						+ "PLZ SMALLINT NOT NULL, "
						+ "adresse VARCHAR(100), "
						+ "kontakt VARCHAR(30));";

		System.out.println("created table herstellerLaster successfully");
		stmt.executeUpdate(tableHerstellerLaster);
		stmt.close();
	}

	public static void createTableStadtbereich() throws SQLException
	{
		stmt = c.createStatement();

		String tableStadtbereich =
				"CREATE TABLE if not exists stadtbereich "
						+ "(idstadtbereich SMALLINT PRIMARY KEY NOT NULL, "
						+ "stadtbereichname VARCHAR(100), "
						+ "PLZ SMALLINT NOT NULL, "
						+ "einsatzTag DATE NOT NULL);";

		System.out.println("created table stadtbereich successfully");
		stmt.executeUpdate(tableStadtbereich);
		stmt.close();
	}

	public static void insertIntoTable(String tablename, String values) throws SQLException{
		try {
			stmt=c.createStatement();
			String inserts=
					"INSERT INTO "
					+tablename
					+" VALUES("
					+values
					+");";
			stmt.executeUpdate(inserts);
			System.out.println("inserted successfully");
			stmt.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void getValues(){
		String values="";
		String pfad = "";
		String dateiname="";
		String zeile=null;
		boolean nextLine=true;
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nBitte Dateipfad zur Datei mit den Werten eingeben:");

		try{
			pfad=console.readLine();
			if(!pfad.equals("")){
				BufferedReader br=new BufferedReader(new FileReader(pfad));

				dateiname = pfad;
				dateiname = dateiname.replaceFirst("\\.txt", "");
				while(dateiname.contains("/")){
					dateiname=dateiname.substring(dateiname.indexOf("/")+1, dateiname.length());
				}
				while(dateiname.contains("\\")){
					dateiname=dateiname.substring(dateiname.indexOf("\\")+1, dateiname.length());
				}

				while (nextLine) {
					zeile=br.readLine();
					if(zeile!=null)
						insertIntoTable(dateiname, zeile);
					else nextLine=false;
				}
				br.close();
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			if(e.getMessage().contains("(Das System kann die angegebene Datei nicht finden)"))
				getValues();
		}
	}



}
