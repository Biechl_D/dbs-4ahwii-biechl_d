import java.sql.SQLException;

import com.j256.ormlite.field.DatabaseField;

public class Muellaster {

    @DatabaseField(id = true)
    private int lasterid;
    @DatabaseField(canBeNull = false)
    private String kennzeichen;
    @DatabaseField(canBeNull = false)
    private int nichtAusleerbareMueltonnen;
    @DatabaseField(canBeNull = false)
    private int hersteller;
    @DatabaseField(canBeNull = false)
    private int fahrenderStadtbereich;
    
    public Muellaster() {
        // default-constructor 
    }
    public Muellaster(int lasterid, String kennzeichen, int nichtAusleerbareMueltonnen, int hersteller, int fahrenderStadtbereich) {
        this.lasterid=lasterid;
        this.kennzeichen=kennzeichen;
        this.nichtAusleerbareMueltonnen=nichtAusleerbareMueltonnen;
        this.hersteller=hersteller;
        this.fahrenderStadtbereich=fahrenderStadtbereich;
        
    }
    public int getlasterid() {
        return lasterid;
    }
    public void setName(int lasterid) {
        this.lasterid=lasterid;
    }
    
    public String getkennzeichen() {
        return kennzeichen;
    }
    public void setkennzeichen(String kennzeichen) {
        this.kennzeichen = kennzeichen;
    }
    
    public int getnichtAusleerbareMueltonnen() {
        return nichtAusleerbareMueltonnen;
    }
    public void setnichtAusleerbareMueltonnen(int nichtAusleerbareMueltonnen) {
        this.nichtAusleerbareMueltonnen = nichtAusleerbareMueltonnen;
    }
    
    public int gethersteller() {
        return hersteller;
    }
    public void sethersteller(int hersteller) {
        this.hersteller = hersteller;
    }
    
    public int getfahrenderStadtbereich() {
        return fahrenderStadtbereich;
    }
    public void set(int fahrenderStadtbereich) {
        this.fahrenderStadtbereich = fahrenderStadtbereich;
    }
}
	
	
	
	/*
	public static void createTableMuellaster() throws SQLException
	{
		stmt = c.createStatement();

		String tableMuellaster =
				"CREATE TABLE if not exists muellaster "
						+ "(lasterID TINYINT PRIMARY KEY NOT NULL, "
						+ "kennzeichen TEXT, "
						+ "nichtAusleerbareMueltonnen INT, "
						+ "hersteller TINYINT NOT NULL, "
						+ "fahrenderStadtbereich SMALLINT NOT NULL);";

		System.out.println("created table muellaster successfully");
		stmt.executeUpdate(tableMuellaster);
		stmt.close();
	}
	*/
	

