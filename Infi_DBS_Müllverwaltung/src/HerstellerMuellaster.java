import java.sql.SQLException;

import com.j256.ormlite.field.DatabaseField;

public class HerstellerMuellaster {

    @DatabaseField(id = true)
    private int idherstellerLaster;
    @DatabaseField(canBeNull = false)
    private String name;
    @DatabaseField(canBeNull = false)
    private int PLZ;
    @DatabaseField(canBeNull = false)
    private String adresse;
    @DatabaseField(canBeNull = false)
    private String kontakt;
    
    public HerstellerMuellaster() {
        // default-constructor 
    }
    public HerstellerMuellaster(int idherstellerLaster, String name, int PLZ, String adresse, String kontakt) {
        this.idherstellerLaster=idherstellerLaster;
        this.name = name;
        this.PLZ=PLZ;
        this.adresse=adresse;
        this.kontakt=kontakt;
        
    }
    public int getidherstellerLaster() {
        return idherstellerLaster;
    }
    public void setidherstellerLaster(int idherstellerLaster) {
        this.idherstellerLaster = idherstellerLaster;
    }
    
    public String getname() {
        return name;
    }
    public void setname(String name) {
        this.name =name ;
    }
    
    public int getPLZ() {
        return PLZ;
    }
    public void setPLZ(int PLZ ) {
        this.PLZ = PLZ;
    }
    
    public String getadresse() {
        return adresse;
    }
    public void setadresse(String adresse) {
        this.adresse = adresse;
    }
    
    public String getkontakt() {
        return kontakt;
    }
    public void setkontakt(String kontakt) {
        this.kontakt = kontakt;
    }
}
	
	
	
	
	
	/*
	public static void createTableHerstellerMuellaster() throws SQLException
	{
		stmt = c.createStatement();

		String tableHerstellerLaster =
				"CREATE TABLE if not exists herstellerLaster "
						+ "(idherstellerLaster TINYINT PRIMARY KEY NOT NULL, "
						+ "name VARCHAR(50), "
						+ "PLZ SMALLINT NOT NULL, "
						+ "adresse VARCHAR(100), "
						+ "kontakt VARCHAR(30));";

		System.out.println("created table herstellerLaster successfully");
		stmt.executeUpdate(tableHerstellerLaster);
		stmt.close();
	}
	*/


