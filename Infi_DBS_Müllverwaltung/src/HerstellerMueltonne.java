import java.sql.SQLException;

import com.j256.ormlite.field.DatabaseField;

public class HerstellerMueltonne {

    @DatabaseField(id = true)
    private int idhersteller;
    @DatabaseField(canBeNull = false)
    private String name;
    @DatabaseField(canBeNull = false)
    private int PLZ;
    @DatabaseField(canBeNull = false)
    private String adresse;
    @DatabaseField(canBeNull = false)
    private String kontakt;
    
    public HerstellerMueltonne() {
        // default-constructor 
    }
    public HerstellerMueltonne(int idhersteller, String name, int PLZ, String adresse, String kontakt) {
        this.idhersteller = idhersteller;
        this.name = name;
        this.PLZ=PLZ;
        this.adresse=adresse;
        this.kontakt=kontakt;
        
    }
    public int getidhersteller() {
        return idhersteller;
    }
    public void setName(int idhersteller) {
        this.idhersteller = idhersteller;
    }
    
    public String getname() {
        return name ;
    }
    public void setname(String name ) {
        this.name = name;
    }
    
    public int getPLZ() {
        return PLZ;
    }
    public void setPLZ(int PLZ ) {
        this.PLZ = PLZ;
    }
    
    public String getadresse() {
        return adresse;
    }
    public void setadresse(String adresse) {
        this.adresse = adresse;
    }
    
    public String getkontakt() {
        return kontakt;
    }
    public void setkontakt(String kontakt) {
        this.kontakt = kontakt;
    }
}
	
	
	
	/*
	public static void createTableHerstellerMueltonne() throws SQLException
	{
		stmt = c.createStatement();

		String tableHerstellerMueltonne =
				"CREATE TABLE if not exists herstellerMueltonne "
						+ "(idhersteller TINYINT PRIMARY KEY NOT NULL, "
						+ "name VARCHAR(50), "
						+ "PLZ SMALLINT NOT NULL, "
						+ "adresse VARCHAR(100), "
						+ "kontakt VARCHAR(30));";

		System.out.println("created table herstellerMueltonne successfully");
		stmt.executeUpdate(tableHerstellerMueltonne);
		stmt.close();
	}
	*/

