import java.sql.SQLException;

import com.j256.ormlite.field.DatabaseField;

public class Mueltonne {

	    @DatabaseField(id = true)
	    private int idmueltonne;
	    @DatabaseField(canBeNull = false)
	    private String modell;
	    @DatabaseField(canBeNull = false)
	    private int fassungsvermoegen;
	    @DatabaseField(canBeNull = false)
	    private int hersteller;
	    
	    public Mueltonne() {
	        // Default constructor
	    }
	    public Mueltonne(int idmueltonne, String modell, int fassungsvermoegen, int hersteller) {
	        this.idmueltonne = idmueltonne;
	        this.modell = modell;
	        this.fassungsvermoegen = fassungsvermoegen;
	        this.hersteller=hersteller; 
	        
	    }
	    public int getidmueltonne() {
	        return idmueltonne;
	    }
	    public void setidmueltonne(int idmueltonne) {
	        this.idmueltonne = idmueltonne;
	    }
	    
	    public String getmodell() {
	        return modell;
	    }
	    public void setmodell(String modell) {
	        this.modell = modell;
	    }
	    
	    public int getfassungsvermoegen() {
	        return fassungsvermoegen;
	    }
	    public void setfassungsvermoegen(int fassungsvermoegen) {
	        this.fassungsvermoegen = fassungsvermoegen;
	    }
	    
	    public int gethersteller() {
	        return hersteller;
	    }
	    public void sethersteller(int hersteller ) {
	        this.hersteller = hersteller;
	    }
	}
	
	




 
          // OHNE ORM	
	/*
	public static void createTableMueltonne() throws SQLException
	{
		stmt =  c.createStatement();

		String tableMueltonne =
				"CREATE TABLE if not exists mueltonne "
						+ "(idmueltonne INT PRIMARY KEY NOT NULL, "
						+ "modell TINYTEXT, "
						+ "fassungsvermoegen SMALLINT NOT NULL, "
						+ "hersteller TINYINT NOT NULL);";

		System.out.println("created table mueltonne successfully");
		stmt.executeUpdate(tableMueltonne);
		stmt.close();
	}
    */	


