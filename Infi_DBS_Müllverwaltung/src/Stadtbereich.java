import java.sql.SQLException;

import com.j256.ormlite.field.DatabaseField;

public class Stadtbereich {

    @DatabaseField(id = true)
    private int idstadtbereich;
    @DatabaseField(canBeNull = false)
    private String stadtbereichname;
    @DatabaseField(canBeNull = false)
    private int PLZ;
    @DatabaseField(canBeNull = false)
    private String einsatzTag;
    
    public Stadtbereich() {
        // default-constructor 
    }
    public Stadtbereich(int idstadtbereich, String stadtbereichname, int PLZ, String einsatzTag) {
        this.idstadtbereich=idstadtbereich;
        this.stadtbereichname = stadtbereichname;
        this.PLZ = PLZ;
        
    }
    public int getidstadtbereich() {
        return idstadtbereich;
    }
    public void setidstadtbereich(int idstadtbereich) {
        this.idstadtbereich = idstadtbereich;
    }
    
    public String getstadtbereichname() {
        return stadtbereichname;
    }
    public void setstadtbereichname(String stadtbereichname) {
        this.stadtbereichname = stadtbereichname;
    }
    
    public int getPLZ() {
        return PLZ;
    }
    public void setPLZ(int PLZ ) {
        this.PLZ = PLZ;
    }
    
    public String geteinsatzTag() {
        return einsatzTag;
    }
    public void seteinsatzTag(String einsatzTag) {
        this.einsatzTag = einsatzTag;
    }
}
	
	/*public static void createTableStadtbereich() throws SQLException
	{
		stmt = c.createStatement();

		String tableStadtbereich =
				"CREATE TABLE if not exists stadtbereich "
						+ "(idstadtbereich SMALLINT PRIMARY KEY NOT NULL, "
						+ "stadtbereichname VARCHAR(100), "
						+ "PLZ SMALLINT NOT NULL, "
						+ "einsatzTag DATE NOT NULL);";

		System.out.println("created table stadtbereich successfully");
		stmt.executeUpdate(tableStadtbereich);
		stmt.close();
	}
	*/
	

