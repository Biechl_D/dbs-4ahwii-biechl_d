import java.beans.Statement;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Scanner;
import java.util.logging.Level;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class Muelverwaltung {
	
		//private final static String DATABASE_URL = "jdbc:sqlite:C:\\Users\\Administrator\\Downloads\\dumpTruck.db";  //<-- fuer sqlite3

		private Dao<Mueltonne, Integer> mueltonneDao;
		private Dao<Muellaster, Integer> muellasterDao;
		private Dao<HerstellerMueltonne, Integer> herstellermueltonneDao;
		private Dao<HerstellerMuellaster, Integer> herstellermuellasterDao;
		private Dao<Stadtbereich, Integer> stadtbereichDao;
		
		public static void main(String[] args) throws Exception {
			// turn our static method into an instance of Main
			new Muelverwaltung().doMain(args);
		}
		
		public void doMain(String[] args) throws Exception {
			
			ConnectionSource connectionSource = null;
			try {
				// create our data-source for the database
				//connectionSource = new JdbcConnectionSource(DATABASE_URL); //<-- fuer sqlite3
				
				Scanner sc= new Scanner(System.in);
				System.out.println("Passwort bitte eingeben: ");
				String pw = sc.nextLine();
				String password = pw;   
				
				connectionSource = new JdbcConnectionSource("jdbc:mysql://localhost:3306/muelverwaltung", "schule", password);  //<--fuer mysql
				// setup our database and DAOs
				setupDatabase(connectionSource);
				// read and write some data
				readWriteData();
				
				System.out.println("\n\nIt seems to have worked\n\n");
			} finally {
				// destroy the data source which should close underlying connections
				if (connectionSource != null) {
					connectionSource.close();
				}
			}

		}

		private void setupDatabase(ConnectionSource connectionSource) throws Exception {

			mueltonneDao = DaoManager.createDao(connectionSource, Mueltonne.class);
			muellasterDao = DaoManager.createDao(connectionSource, Muellaster.class);
			herstellermueltonneDao = DaoManager.createDao(connectionSource, HerstellerMueltonne.class);
			herstellermuellasterDao = DaoManager.createDao(connectionSource, HerstellerMuellaster.class);
			stadtbereichDao = DaoManager.createDao(connectionSource, Stadtbereich.class);
			
			TableUtils.createTable(connectionSource, Mueltonne.class);
			TableUtils.createTable(connectionSource, Muellaster.class);
			TableUtils.createTable(connectionSource, HerstellerMueltonne.class);
			TableUtils.createTable(connectionSource, HerstellerMuellaster.class);
			TableUtils.createTable(connectionSource, Stadtbereich.class);
		}

		private void readWriteData() throws Exception {

			Mueltonne mueltonne1 = new Mueltonne(1, "Volvo", 6000 , 1);
			Muellaster muellaster1 = new Muellaster(1, "IL 722K", 1, 3, 2);
			HerstellerMueltonne herstellerMT1 = new HerstellerMueltonne(1, "herstellerTonne", 6060, "tonnenstraße22", "herstellerMT@gmail.com");
			HerstellerMuellaster herstellerML1 = new HerstellerMuellaster(3, "herstellerLaster", 6067, "lasterstraße11", "hLaster@gmail.com");
			Stadtbereich sb1 = new Stadtbereich(2, "Hall i. T.", 6060, "22.03.2018");
			
			mueltonneDao.create(mueltonne1);
			muellasterDao.create(muellaster1);
			herstellermueltonneDao.create(herstellerMT1);
			herstellermuellasterDao.create(herstellerML1);
			stadtbereichDao.create(sb1);
		}
        
	    
	   
	      // OHNE ORM
	    /* 
        String user = "root";
		Scanner sc= new Scanner(System.in);
		System.out.println("Paasswort bitte eingeben: ");
		String pw = sc.nextLine();
		String password = pw;   
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Creating DBConnection"); 
			c = DriverManager.getConnection(url, user, password); 
			
			stmt=c.createStatement();
			String database="CREATE DATABASE IF NOT EXISTS muellunternehmen;";
			stmt.executeUpdate(database);
			stmt.close();
			
			stmt=c.createStatement();
			database="USE muellunternehmen";
			stmt.executeUpdate(database);
			stmt.close();

			Mueltonne tonne1 = new Mueltonne();
			Muellaster ml1 = new Muellaster();
			HerstellerMueltonne hmt1 = new HerstellerMueltonne();
			
			
			createTableMueltonne();
			createTableMuellaster();
			createTableHerstellerMueltonne();
			createTableHerstellerMuellaster();
			createTableStadtbereich();
			getValues();

			connectionSource.close();
			//c.close();

		}
		catch(Exception e)
		{
			System.err.println("Couldn't create DBConnection"); 
			System.err.println(e.getClass().getName()+": "+ e.getMessage());
			System.exit(0);
		}
	}

	public static void insertIntoTable(String tablename, String values) throws SQLException{
		try {
			stmt=c.createStatement();
			String inserts=
					"INSERT INTO "
					+tablename
					+" VALUES("
					+values
					+");";
			stmt.executeUpdate(inserts);
			System.out.println("inserted successfully");
			stmt.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void getValues(){
		String values="";
		String pfad = "";
		String dateiname="";
		String zeile=null;
		boolean nextLine=true;
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nBitte Dateipfad zur Datei mit den Werten eingeben:");

		try{
			pfad=console.readLine();
			if(!pfad.equals("")){
				BufferedReader br=new BufferedReader(new FileReader(pfad));

				dateiname = pfad;
				dateiname = dateiname.replaceFirst("\\.txt", "");
				while(dateiname.contains("/")){
					dateiname=dateiname.substring(dateiname.indexOf("/")+1, dateiname.length());
				}
				while(dateiname.contains("\\")){
					dateiname=dateiname.substring(dateiname.indexOf("\\")+1, dateiname.length());
				}

				while (nextLine) {
					zeile=br.readLine();
					if(zeile!=null)
						insertIntoTable(dateiname, zeile);
					else nextLine=false;
				}
				br.close();
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			if(e.getMessage().contains("(Das System kann die angegebene Datei nicht finden)"))
				getValues();
		}
	}

*/

}
