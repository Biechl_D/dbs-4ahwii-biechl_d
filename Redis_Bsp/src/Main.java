import java.util.List;

import redis.clients.jedis.Jedis;

public class Main {

	public static void main(String[] args) {

		//Connecting to Redis server on localhost 
		Jedis jedis = new Jedis("localhost"); 
		jedis.connect();
		System.out.println("Connection to server sucessfully"); 
		//check whether server is running or not 
		System.out.println("Server is running: "+jedis.ping()); 
		jedis.lpush("Muellaster", "Volvo", "XF100", "IL 233");
		jedis.lpush("Muellaster", "MAN", "M500", "IL 342");
		jedis.lpush("Mueltonne", "mueltonne1", "model1");

		List<String> lasterList = jedis.lrange("Muellaster", 0 ,3);
		List<String> tonneList = jedis.lrange("Mueltonne", 0 ,3);
		
		for(int i = 0; i<lasterList.size(); i++) { 
	         System.out.println("Stored Muellaster in redis: "+lasterList.get(i)); 
	      } 
		
		for(int i = 0; i<tonneList.size(); i++) { 
	         System.out.println("Stored Mueltonne in redis: "+tonneList.get(i)); 
	      } 
		
		
	}

}
